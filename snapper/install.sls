{% from "snapper/map.jinja" import map with context %}

snapper_install:
  pkg.installed:
    - install_recommends: False
    - pkgs: 
        {{map.pkgs}}
