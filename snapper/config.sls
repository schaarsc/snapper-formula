{% from "snapper/map.jinja" import map with context %}

{% import_yaml 'snapper/defaults.yaml' as yaml_defaults %}
{% set pillar_defaults = salt['pillar.get']('snapper:defaults', {}) %}
{% set defaults = salt['defaults.merge'](yaml_defaults, pillar_defaults) %}

{% for name,config in salt['pillar.get']('snapper:configs', {}).items() %}
snapper_create_{{name}}:
  cmd.run:
    - runas: root
    - name: /usr/bin/snapper --config {{name}} create-config {{config.subvolume}}
    - onlyif: /usr/bin/test ! -d {{config.subvolume}}/.snapshots/

snapper_config_{{name}}:
  file.managed:
    - name: {{map.configs_dir}}/{{name}}
    - mode: 640
    - user: root
    - group: root
    - template: jinja
    - source: salt://snapper/files/template.jinja
    - context:
        {{ salt['defaults.merge'](defaults, config) }}
{% endfor %}
