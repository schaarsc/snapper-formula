snapper formula
===============
Create and manager snapper configurations.  
Note: The formula will only create the snapper configuration, not the subvolume.


Available States
----------------
* snapper  
  Meta-state includes all other states
* snapper.install  
  Install snapper SW with package manager
* snapper.config  
  Manage configurations defined in pillar

Alternative
-----------
For onetime configuration use [salt.modules.snapper](https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.snapper.html)